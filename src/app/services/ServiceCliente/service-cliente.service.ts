import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ServiceClienteService {

  public api = "http://localhost:3900";

  constructor(private _http : HttpClient) { }

  obtenerCliente(){
    const url = `${this.api}/obtener/cliente`;
    console.log(url);
    return this._http.get(url);
  }

  agregarCliente(body : any){
    const url = `${this.api}/cliente/nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }

  eliminarCliente(id : any){
    const url = `${this.api}/borrar/cliente/${id}`;
    console.log(url);
    return this._http.delete(url);
  }

  actualizarCliente(id : any, body : any){
    const url = `${this.api}/update/cliente/${id}`;
    console.log(url);
    return this._http.put(url, body);
  }
  informacionClienteId(id : any){
    const url = `${this.api}/obtener/cliente/${id}`;
    console.log(url);
    return this._http.get(url)
  }


}
