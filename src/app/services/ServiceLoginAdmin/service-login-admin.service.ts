import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceLoginAdminService {

  public api = "http://localhost:3900";

  constructor(private _http : HttpClient) { }
  obtenerPersonal(){
    const url = `${this.api}/obtener/empleados/`;
    console.log(url);
    return this._http.get(url);
  }
  
  informacionId(nom : any){
    const url = `${this.api}/buscar/usuario/correo/${nom}`;
    console.log(url);
    return this._http.get(url)
  }
  obtenerLoginAdmin(){
    const url = `${this.api}/obtener/LoginAdmin/`;
    console.log(url);
    return this._http.get(url);
  }

  agregarLoginAdmin(body : any){
    const url = `${this.api}/LoginAdmin
    /nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }

  agregarAdmin(body : any){
    const url = `${this.api}/administrador/nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }
  eliminarLoginAdmin(id : any){
    const url = `${this.api}/eliminar/LoginAdmin
    /${id}`;
    console.log(url);
    return this._http.delete(url);
  }

  actualizarLoginAdmin(id : any, body : any){
    const url = `${this.api}/editar/LoginAdmin
    /${id}`;
    console.log(url);
    return this._http.put(url, body);
  }
  informacionLoginAdmin(id : any){
    const url = `${this.api}/informacion/loginAdmin/${id}`;
    console.log(url);
    return this._http.get(url)
  }

}
