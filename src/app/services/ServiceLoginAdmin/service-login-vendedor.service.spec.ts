import { TestBed } from '@angular/core/testing';

import { ServiceLoginVendedorService } from './service-login-vendedor.service';

describe('ServiceLoginVendedorService', () => {
  let service: ServiceLoginVendedorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceLoginVendedorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
