import { TestBed } from '@angular/core/testing';

import { ServiceLoginUsuarioService } from './service-login-usuario.service';

describe('ServiceLoginUsuarioService', () => {
  let service: ServiceLoginUsuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceLoginUsuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
