import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceLoginUsuarioService {


  public api = "http://localhost:3900";

  constructor(private _http : HttpClient) { }
  
  obtenerPersonal(){
    const url = `${this.api}/obtener/empleados/`;
    console.log(url);
    return this._http.get(url);
  }
  informacionId(nom : any){
    const url = `${this.api}/usuario/correo/${nom}`;
    console.log(url);
    return this._http.get(url)
  }
  obtenerLoginUsuario(){
    const url = `${this.api}/obtener/loginUsuario/`;
    console.log(url);
    return this._http.get(url);
  }

  agregarLoginUsuario(body : any){
    const url = `${this.api}/loginUsuario
    /nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }
  agregarUsuario(body : any){
    const url = `${this.api}/usu/nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }
  eliminarLoginUsuario(id : any){
    const url = `${this.api}/eliminar/loginUsuario
    /${id}`;
    console.log(url);
    return this._http.delete(url);
  }

  actualizarLoginUsuario(id : any, body : any){
    const url = `${this.api}/editar/loginUsuario
    /${id}`;
    console.log(url);
    return this._http.put(url, body);
  }
  informacionLoginUsuario(id : any){
    const url = `${this.api}/informacion/loginUsuario/${id}`;
    console.log(url);
    return this._http.get(url)
  }
  
}
