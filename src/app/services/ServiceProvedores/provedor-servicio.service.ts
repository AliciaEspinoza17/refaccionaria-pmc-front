import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProvedorServicioService {

  public api1 = "http://localhost:3900";

  constructor(private _http : HttpClient) { }

  obtenerProvedor(){
    const url = `${this.api1}/obtener/proveedor`;
    console.log(url);
    return this._http.get(url);
  }
 

  agregarProvedor(body : any){
    const url = `${this.api1}/proveedor/nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }

  eliminarProvedor(id : any){
    const url = `${this.api1}/borrar/proveedor/${id}`;
    console.log(url);
    return this._http.delete(url);
  }
  informacionProveedorId(id : any){
    const url = `${this.api1}/obtener/proveedor/${id}`;
    console.log(url);
    return this._http.get(url)
  }

  actualizarProvedor(id : any, body : any){
    const url = `${this.api1}/update/proveedor/${id}`;
    console.log(url);
    return this._http.put(url, body);
  }
}
