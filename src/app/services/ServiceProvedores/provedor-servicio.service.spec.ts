import { TestBed } from '@angular/core/testing';

import { ProvedorServicioService } from './provedor-servicio.service';

describe('ProvedorServicioService', () => {
  let service: ProvedorServicioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProvedorServicioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
