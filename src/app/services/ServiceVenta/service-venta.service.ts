import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceVentaService {

  public api = "http://localhost:3900";

  constructor(private _http: HttpClient) { }
  informacionProductoId(id : any){
    const url = `${this.api}/obtener/producto/${id}`;
    console.log(url);
    return this._http.get(url)
  }
  obtenerCliente(){
    const url = `${this.api}/obtener/cliente`;
    console.log(url);
    return this._http.get(url);
  }

  obtenerVenta() {
    const url = `${this.api}/obtener/producto`;
    console.log(url);
    return this._http.get(url);
  }
  actualizarProducto(id : any, body : any){
    const url = `${this.api}/update/producto/${id}`;
    console.log(url);
    return this._http.put(url, body);
  }
  agregarVenta(body: any) {
    const url = `${this.api}/venta/nueva`;
    console.log(url);
    return this._http.post(url, body);
  }

  eliminarVenta(id: any) {
    const url = `${this.api}/eliminar/venta/${id}`;
    console.log(url);
    return this._http.delete(url);
  }

  actualizarVenta(id: any, body: any) {
    const url = `${this.api}/editar/venta/${id}`;
    console.log(url);
    return this._http.put(url, body);
  }
}
