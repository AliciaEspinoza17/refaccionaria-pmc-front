import { TestBed } from '@angular/core/testing';

import { ServiceVentaService } from './service-venta.service';

describe('ServiceVentaService', () => {
  let service: ServiceVentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceVentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
