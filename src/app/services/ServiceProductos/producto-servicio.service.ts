import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductoServicioService {

  public api = "http://127.0.0.1:3900";

  constructor( private _http:HttpClient) { 
  }

  agregarProducto(body : any){
    const url = `${this.api}/producto/nuevo`;
    console.log(url);
    return this._http.post(url, body);
  }
  obtenerProvedor(){
    const url = `${this.api}/obtener/proveedor`;
    console.log(url);
    return this._http.get(url);
  }
  informacionProductoId(id : any){
    const url = `${this.api}/obtener/producto/${id}`;
    console.log(url);
    return this._http.get(url)
  }

  obtenerProductos(){
    const url = `${this.api}/obtener/producto`;
    console.log(url);
    return this._http.get(url);
  }

  eliminarProducto(id : any){
    const url = `${this.api}/borrar/producto/${id}`;
    console.log(url);
    return this._http.delete(url);
  }

  actualizarProducto(id : any, body : any){
    const url = `${this.api}/update/producto/${id}`;
    console.log(url);
    return this._http.put(url, body);
  }

}