import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerProductosComponent } from './components/ver-productos/ver-productos.component';
import { VerProvedorComponent } from './components/ver-provedor/ver-provedor.component';
import { AgregarProductoComponent } from './components/agregar-producto/agregar-producto.component';
import { EditarProductoComponent } from './components/editar-producto/editar-producto.component';
import { AgregarProveedorComponent } from './components/agregar-proveedor/agregar-proveedor.component';
import { VentaComponent } from './components/venta/venta.component';
import { AgregarClienteComponent } from './components/agregar-cliente/agregar-cliente.component';
import { LoginAdminComponent } from './components/login-admin/login-admin.component';
import { VerClienteComponent } from './components/ver-cliente/ver-cliente.component';
import { LoginUsuarioComponent } from './components/login-usuario/login-usuario.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { EditarProveedorComponent } from './components/editar-proveedor/editar-proveedor.component';
import { AgregarAdminComponent } from './components/agregar-admin/agregar-admin.component';
import { AgregarUsuarioComponent } from './components/agregar-usuario/agregar-usuario.component';
import { MemotlComponent } from './components/memotl/memotl.component';

 
const routes: Routes = [
    {
        path : 'Inicio/LoginAdmin',
        component : LoginAdminComponent
    },
    {
        path : 'Inicio/AgregarAdmin',
        component : AgregarAdminComponent
    },
    {
        path : 'Inicio/LoginUsuario',
        component : LoginUsuarioComponent
    },
    {
        path : 'Inicio/AgregarUsuario',
        component : AgregarUsuarioComponent
    },
    {

        path : 'Inicio/Productos',
        component : VerProductosComponent
    },
    {
        path : 'Inicio/Cliente',
        component : VerClienteComponent
    },
    {
        path : 'Inicio/Provedores',
        component : VerProvedorComponent
    },
  
    {
        path : 'Inicio/Venta',
        component : VentaComponent
    },
    {
        path : 'Agregar/Producto',
        component : AgregarProductoComponent
    },
    {
        path : 'Agregar/Cliente',
        component : AgregarClienteComponent
    },
    {
        path : 'Agregar/Proveedor',
        component : AgregarProveedorComponent
    },
    {
        path : 'Editar/Cliente',
        component : EditarClienteComponent
    },
    {
        path : 'Editar/Producto',
        component : EditarProductoComponent
    },
    
    {
        path : 'Inicio/Venta',
        component : VentaComponent
    },
    {
        path : 'Editar/Proveedor',
        component : EditarProveedorComponent
    },
    {
        path: 'Editar/memotl',
        component : MemotlComponent
    },
    
    {
        path : '',
        redirectTo : 'Inicio/login',
        pathMatch : 'full',

    },
    { path: '**', component: LoginAdminComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }