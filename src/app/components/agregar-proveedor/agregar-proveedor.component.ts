import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ProvedorServicioService } from 'src/app/services/ServiceProvedores/provedor-servicio.service';


@Component({
  selector: 'app-agregar-proveedor',
  templateUrl: './agregar-proveedor.component.html',
  styleUrls: ['./agregar-proveedor.component.css']
})
export class AgregarProveedorComponent {

  datos : any [] =[]

  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ#.@\s]+$/;
  numeros = /^[1-9]\d*(\.\d+)?$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;
  ok4 = true;

  newSchemaProveedor = {
    nombreProveedor : '',
    telefonoProveedor : '',
    correoProveedor: '',
    direccionProveedor: '',
  }

   constructor(private _Proveedor : ProvedorServicioService, private router : Router){}

  ngOnInit(): void {
  }

  cargarProvedor(){
    this._Proveedor.obtenerProvedor().subscribe((data : any ) => {
      this.datos = data.respuesta;
      console.log(this.datos);
    })
  }

  addProvedor(){
    this.ok1 = this.letras.test(this.newSchemaProveedor.nombreProveedor);
    this.ok2 = this.numeros.test(this.newSchemaProveedor.telefonoProveedor);
    this.ok3 = this.letras.test(this.newSchemaProveedor.correoProveedor);
    this.ok4 = this.letras.test(this.newSchemaProveedor.direccionProveedor);
    if ([this.ok1, this.ok2, this.ok3, this.ok4].every(val => val === true)){
      this._Proveedor.agregarProvedor(this.newSchemaProveedor).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡Provedor ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/Provedores']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/Provedor']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }
}
