import { Component, OnInit } from '@angular/core';
import { ProvedorServicioService } from 'src/app/services/ServiceProvedores/provedor-servicio.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ver-provedor',
  templateUrl: './ver-provedor.component.html',
  styleUrls: ['./ver-provedor.component.css']
})
export class VerProvedorComponent implements OnInit{

  datos : any [] = [];

  constructor(private _provedor : ProvedorServicioService, private router: Router){}

  ngOnInit(): void {
    this.cargarProvedores()
  }

  cargarProvedores(){
    this._provedor.obtenerProvedor().subscribe((data : any ) => {
      this.datos = data.respuesta;
      console.log(this.datos);
    })
  }

  borrarProveedor(id:any){
    Swal.fire({
      title: '¡ADVERTENCIA!',
      text: '¿Desea eliminar de forma permanente el producto?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#31f700',
      cancelButtonColor: '#f70000',
      confirmButtonText: 'ACEPTAR',
      cancelButtonText: 'CANCELAR'
    }).then((result) => {
      if (result.isConfirmed) {
        this._provedor.eliminarProvedor(id).subscribe((data : any) => {
          console.log(data);
          this.cargarProvedores();
        })
      }
    })
  }

  guardarId(id : any){
    const objectId = id;
    localStorage.setItem('objectId', objectId);
    console.log("Id del proveedor: ", objectId);
  }

}

