import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductoServicioService } from 'src/app/services/ServiceProductos/producto-servicio.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ver-productos',
  templateUrl: './ver-productos.component.html',
  styleUrls: ['./ver-productos.component.css']
})
export class VerProductosComponent implements OnInit{

  datos: any[] = [];

  constructor(private _productos:ProductoServicioService, private router: Router){}

  ngOnInit(): void {
    this.cargarProductos();
  } 

  cargarProductos(){
    this._productos.obtenerProductos().subscribe((data : any ) => {
      this.datos = data.respuesta;
      console.log(this.datos);
    })
  }

  borrarProducto(id:any){

    Swal.fire({
      title: '¡ADVERTENCIA!',
      text: '¿Desea eliminar de forma permanente el producto?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#31f700',
      cancelButtonColor: '#f70000',
      confirmButtonText: 'ACEPTAR',
      cancelButtonText: 'CANCELAR'
    }).then((result) => {
      if (result.isConfirmed) {
        this._productos.eliminarProducto(id).subscribe((data : any)=>{
          console.log(data);
          this.cargarProductos();
        })
      }
    })
  }

  guardarId(id : any){
    const objectId = id;
    localStorage.setItem('objectId', objectId);
    console.log("Id del usuario: ",objectId); 
  }

}
