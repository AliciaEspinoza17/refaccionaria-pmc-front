import { Component } from '@angular/core';
import { ServiceClienteService } from 'src/app/services/ServiceCliente/service-cliente.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ver-cliente',
  templateUrl: './ver-cliente.component.html',
  styleUrls: ['./ver-cliente.component.css']
})
export class VerClienteComponent {
  datos: any[] = [];

  constructor(private _cliente: ServiceClienteService, private router: Router) { }


  ngOnInit(): void {
    this.cargarCliente();
  } 

  cargarCliente(){
    this._cliente.obtenerCliente().subscribe((data : any ) => {
      this.datos = data.respuesta;
      console.log(this.datos);
    })
  }

  borrarCliente(id:any){

    Swal.fire({
      title: '¡ADVERTENCIA!',
      text: '¿Desea eliminar de forma permanente el producto?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#31f700',
      cancelButtonColor: '#f70000',
      confirmButtonText: 'ACEPTAR',
      cancelButtonText: 'CANCELAR'
    }).then((result) => {
      if (result.isConfirmed) {
        this._cliente.eliminarCliente(id).subscribe((data : any)=>{
          console.log(data);
          this.cargarCliente();
        })
      }
    })
  }

  guardarId(id : any){
    const objectId = id;
    localStorage.setItem('objectId', objectId);
    console.log("Id del usuario: ",objectId); 
  }
}