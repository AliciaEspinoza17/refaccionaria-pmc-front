import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ServiceLoginAdminService } from 'src/app/services/ServiceLoginAdmin/service-login-admin.service';


@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent {
  datos : any [] =[]
  password=""
  rol=""
  nom=""
  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ\s]+$/;
  numeros = /^[1-9]\d*(\.\d+)?$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;

  newSchemaAdmi = {
    nombreAdm : '',
    passwordAdm: '',
    role: '',
  }

  constructor(private _LoginAdmin : ServiceLoginAdminService , private router : Router){}

  ngOnInit(): void {
  }
  guardardatos(){
    this.nom=this.newSchemaAdmi.nombreAdm
    this.datosUsu(this.nom)
  }
  datosUsu(nom: any) {
    this._LoginAdmin.informacionId(this.nom).subscribe((data: any) => {
      this.datos = [data.DatosInicioSesion];
      this.password=data.DatosInicioSesion.passwordAdm
      console.log(this.password);
      console.log(this.newSchemaAdmi.passwordAdm);
      
      if (this.newSchemaAdmi.passwordAdm==this.password) {
        this.router.navigate(['Inicio/Productos']).then(()=>{
          window.location.reload()
        })
        
      }else{
        alertaError('Error en el inicio de sesión. Correo o contraseña incorrectos.');
      }
    },(error: any) => {
      alertaError('Error en el inicio de sesión. Correo o contraseña incorrectos.');
    });
}
  cargarLoginAdmin(){
    this._LoginAdmin.obtenerLoginAdmin().subscribe((data : any ) => {
      this.datos = data.Clientes;
      console.log(this.datos);
    })
  }

  addLoginAdmin(){
    this.ok1 = this.letras.test(this.newSchemaAdmi.nombreAdm);
    this.ok2 = this.letras.test(this.newSchemaAdmi.passwordAdm);
    this.ok3 = this.letras.test(this.newSchemaAdmi.role);
    if ([this.ok1, this.ok2, this.ok3].every(val => val === true)){
      this._LoginAdmin.agregarLoginAdmin(this.newSchemaAdmi).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡Cliente ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/LoginAdmin']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/LoginAdmin']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }
}
function alertaError(message: string) {
  setTimeout(() => {
    Swal.fire({
      icon: 'error',
      title: '¡Error!',
      text: message,
      confirmButtonColor: '#561113',
      confirmButtonText: 'Aceptar'
    });
  }, 50);
  }