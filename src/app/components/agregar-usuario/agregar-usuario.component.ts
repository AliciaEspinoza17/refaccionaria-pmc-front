import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceLoginUsuarioService } from 'src/app/services/ServiceLoginUsuario/service-login-usuario.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-agregar-usuario',
  templateUrl: './agregar-usuario.component.html',
  styleUrls: ['./agregar-usuario.component.css']
})
export class AgregarUsuarioComponent {
  datos : any [] =[]
  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ#.\s]+$/;
  numeros = /^[1-9]\d*(\.\d+)?$/;
  newSchemaCliente = {
    nombreUsu : '',
    passwordUsu: '',
  }
  ok1 = true;
  constructor(private _Cliente : ServiceLoginUsuarioService , private router : Router){}
  addCliente(){
    this.ok1 = this.letras.test(this.newSchemaCliente.nombreUsu);
    if ([this.ok1].every(val => val === true)){
      this._Cliente.agregarUsuario(this.newSchemaCliente).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡Cliente ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/LoginUsuario']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/Cliente']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }
}
