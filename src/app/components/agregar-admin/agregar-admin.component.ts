import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceLoginAdminService } from 'src/app/services/ServiceLoginAdmin/service-login-admin.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-agregar-admin',
  templateUrl: './agregar-admin.component.html',
  styleUrls: ['./agregar-admin.component.css']
})
export class AgregarAdminComponent {
  datos : any [] =[]
  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ#.\s]+$/;
  numeros = /^[1-9]\d{1,10}$/;
  newSchemaCliente = {
    nombreAdm : '',
    passwordAdm: '',
  }
  ok1 = true;
  constructor(private _LoginAdmin : ServiceLoginAdminService , private router : Router){}
  addCliente(){
    this.ok1 = this.letras.test(this.newSchemaCliente.nombreAdm);
    
    if ([this.ok1].every(val => val === true)){
      this._LoginAdmin.agregarAdmin(this.newSchemaCliente).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡ADMINISTRADOR ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['Inicio/LoginAdmin']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/Cliente']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }
}
