import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ProvedorServicioService } from 'src/app/services/ServiceProvedores/provedor-servicio.service';


@Component({
  selector: 'app-editar-proveedor',
  templateUrl: './editar-proveedor.component.html',
  styleUrls: ['./editar-proveedor.component.css']
})
export class EditarProveedorComponent {
  objectId = localStorage.getItem('objectId');
  ProveedorDatos : any [] = [];
  datos : any [] = [];

  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ#.@\s]+$/;
  numeros = /^[1-9]\d{1,10}$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;
  ok4 = true;

  newSchemaProveedor = {
    nombreProveedor : '',
    telefonoProveedor: '',
    correoProveedor: '',
    direccionProveedor:'',
  }

  constructor(private _Proveedor : ProvedorServicioService, private router : Router){}

  ngOnInit(): void {
    this.prerellenarDatos(this.objectId);
  }

  prerellenarDatos(id : any){
    this._Proveedor.informacionProveedorId(this.objectId).subscribe((data : any ) => {
      this.ProveedorDatos = [data.respuesta];
      console.log("Datos del Proveedor: ", this.ProveedorDatos);

      this.newSchemaProveedor = {
        nombreProveedor : this.ProveedorDatos[0].nombreProveedor,
        telefonoProveedor: this.ProveedorDatos[0].telefonoProveedor,
        correoProveedor: this.ProveedorDatos[0].correoProveedor,
        direccionProveedor: this.ProveedorDatos[0].direccionProveedor,
      };
    });
  }

  cargarProveedor(){
    this._Proveedor.obtenerProvedor().subscribe((data : any ) => {
      this.datos = data.Clientes;
      console.log(this.datos);
    })
  }

  updateProveedor(){

    this.ok1 = this.letras.test(this.newSchemaProveedor.nombreProveedor);
    this.ok2 = this.numeros.test(this.newSchemaProveedor.telefonoProveedor);
    this.ok3 = this.letras.test(this.newSchemaProveedor.correoProveedor);
    this.ok4 = this.letras.test(this.newSchemaProveedor.direccionProveedor);


    if ([this.ok1, this.ok2, this.ok3].every(val => val === true)){
      
      this._Proveedor.actualizarProvedor(this.objectId, this.newSchemaProveedor).subscribe((data : any) =>{
        console.log(data);

        Swal.fire({
          icon: 'success',
          title: '¡Cliente ALMACENADO!',
          timer : 20000

        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/Provedores']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })


    }else{

      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });
      
      setTimeout(() => {
        this.router.navigate(['/Editar/Proveedor']).then(() => {
          window.location.reload();
        });
      }, 1000); 
      
    }
  }
}
