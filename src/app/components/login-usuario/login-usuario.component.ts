import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ServiceLoginUsuarioService } from 'src/app/services/ServiceLoginUsuario/service-login-usuario.service';

@Component({
  selector: 'app-login-usuario',
  templateUrl: './login-usuario.component.html',
  styleUrls: ['./login-usuario.component.css']
})
export class LoginUsuarioComponent {
  datos : any [] =[]
  password=""
  rol=""
  nom=""
  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ\s]+$/;
  numeros = /^[1-9]\d*(\.\d+)?$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;

  newSchemaUsu = {
    nombreUsu : '',
    passwordUsu: '',
  }

  constructor(private _LoginUsuario : ServiceLoginUsuarioService , private router : Router){}

  ngOnInit(): void {
  }
  guardardatos(){
    this.nom=this.newSchemaUsu.nombreUsu
    this.datosUsu(this.nom)
  }

  datosUsu(nom: any) {
    this._LoginUsuario.informacionId(this.nom).subscribe((data: any) => {
      this.datos = [data.DatosInicioSesion];
      this.password=data.DatosInicioSesion.passwordUsu
      if (this.newSchemaUsu.passwordUsu==this.password) {
        this.router.navigate(['Inicio/Venta']).then(()=>{
          window.location.reload()
        })
      } else{
        alertaError('Error en el inicio de sesión. Correo o contraseña incorrectos.');
      }
    }
    ,(error: any) => {
      alertaError('Error en el inicio de sesión. Correo o contraseña incorrectos.');
    });
}
  cargarLoginUsuario(){
    this._LoginUsuario.obtenerLoginUsuario().subscribe((data : any ) => {
      this.datos = data.Usuario;
      console.log(this.datos);
    })
  }

  addLoginUsuario(){
    this.ok1 = this.letras.test(this.newSchemaUsu.nombreUsu);
    this.ok2 = this.letras.test(this.newSchemaUsu.passwordUsu);
    
    if ([this.ok1, this.ok2, this.ok3].every(val => val === true)){
      this._LoginUsuario.agregarLoginUsuario(this.newSchemaUsu).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡Cliente ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/LoginUsuario']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/LoginUsuario']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }

}
function alertaError(message: string) {
  setTimeout(() => {
    Swal.fire({
      icon: 'error',
      title: '¡Error!',
      text: message,
      confirmButtonColor: '#561113',
      confirmButtonText: 'Aceptar'
    });
  }, 50);
  }