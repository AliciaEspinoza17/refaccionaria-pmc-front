import { Component, OnInit } from '@angular/core';
import { ProductoServicioService } from 'src/app/services/ServiceProductos/producto-servicio.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-producto',
  templateUrl: './editar-producto.component.html',
  styleUrls: ['./editar-producto.component.css']
})
export class EditarProductoComponent implements OnInit{

  objectId = localStorage.getItem('objectId');
  productoDatos : any [] = [];
  datos : any [] = [];
  datosc : any [] = [];

  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ\s]+$/;
  numeros = /^[1-9]\d{1,10}$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;
  ok4 = true;
  ok5 = true;
  ok6 = true;
  ok7 = true;

  newSchemaProducto = {
    nombreProducto : '',
    marcaProducto : '',
    presentacionProducto:'',
    contenidoProducto:'',
    precioProducto: '',
    descripcionProducto: '',
    proovedor: '',
  }

  constructor(private _Producto : ProductoServicioService, private router : Router){}

  ngOnInit(): void {
    this.prerellenarDatos(this.objectId);
    this.cargarprov()
  }

  prerellenarDatos(id : any){
    this._Producto.informacionProductoId(this.objectId).subscribe((data : any ) => {
      this.productoDatos = [data.respuesta];
      console.log("Datos del producto: ", this.productoDatos);

      this.newSchemaProducto = {
        nombreProducto : this.productoDatos[0].nombreProducto,
        marcaProducto: this.productoDatos[0].marcaProducto,
        presentacionProducto : this.productoDatos[0].presentacionProducto,
        contenidoProducto: this.productoDatos[0].contenidoProducto,
        precioProducto : this.productoDatos[0].precioProducto,
        descripcionProducto: this.productoDatos[0].descripcionProducto,
        proovedor:this.productoDatos[0].pr
      };
    });
  }
  cargarprov(){
    this._Producto.obtenerProvedor().subscribe((data: any) => {
      this.datosc = data.respuesta;
      console.log(this.datosc);
    })
  }
  cargarProductos(){
    this._Producto.obtenerProductos().subscribe((data : any ) => {
      this.datos = data.productos;
      console.log(this.datos);
    })
  }

  updateProducto(){

    this.ok1 = this.letras.test(this.newSchemaProducto.nombreProducto);
    this.ok2 = this.letras.test(this.newSchemaProducto.marcaProducto);
    this.ok3 = this.letras.test(this.newSchemaProducto.presentacionProducto);
    this.ok4 = this.letras.test(this.newSchemaProducto.contenidoProducto);
    this.ok5 = this.letras.test(this.newSchemaProducto.precioProducto);
    this.ok6 = this.letras.test(this.newSchemaProducto.descripcionProducto);
    



    if ([this.ok1, this.ok2, this.ok3, this.ok4, this.ok5, this.ok6].every(val => val === true)){
      
      this._Producto.actualizarProducto(this.objectId, this.newSchemaProducto).subscribe((data : any) =>{
        console.log(data);
        

        Swal.fire({
          icon: 'success',
          title: '¡PRODUCTO ALMACENADO!',
          timer : 20000

        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/Productos']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })


    }else{

      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });
      
      setTimeout(() => {
        this.router.navigate(['/Editar/Producto']).then(() => {
          window.location.reload();
        });
      }, 1000); 
      
    }
  }
}