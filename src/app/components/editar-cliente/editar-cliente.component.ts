import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ServiceClienteService } from 'src/app/services/ServiceCliente/service-cliente.service';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent {
  objectId = localStorage.getItem('objectId');
  ClienteDatos : any [] = [];
  datos : any [] = [];

  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ#.\s]+$/;
  numeros = /^[1-9]\d*(\.\d+)?$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;

  newSchemaCliente = {
    nombreCliente : '',
    telefonoCliente: '',
    direccionCliente: '',
  }

  constructor(private _Cliente : ServiceClienteService, private router : Router){}

  ngOnInit(): void {
    this.prerellenarDatos(this.objectId);
  }

  prerellenarDatos(id : any){
    this._Cliente.informacionClienteId(this.objectId).subscribe((data : any ) => {
      this.ClienteDatos = [data.respuesta];
      console.log("Datos del Cliente: ", this.ClienteDatos);

      this.newSchemaCliente = {
        nombreCliente : this.ClienteDatos[0].nombreCliente,
        telefonoCliente: this.ClienteDatos[0].telefonoCliente,
        direccionCliente: this.ClienteDatos[0].direccionCliente,
      };
    });
  }

  cargarClientes(){
    this._Cliente.obtenerCliente().subscribe((data : any ) => {
      this.datos = data.Clientes;
      console.log(this.datos);
    })
  }

  updateCliente(){

    this.ok1 = this.letras.test(this.newSchemaCliente.nombreCliente);
    this.ok2 = this.letras.test(this.newSchemaCliente.telefonoCliente);
    this.ok3 = this.letras.test(this.newSchemaCliente.direccionCliente);



    if ([this.ok1, this.ok2, this.ok3].every(val => val === true)){
      
      this._Cliente.actualizarCliente(this.objectId, this.newSchemaCliente).subscribe((data : any) =>{
        console.log(data);

        Swal.fire({
          icon: 'success',
          title: '¡Cliente ALMACENADO!',
          timer : 20000

        });
        
        setTimeout(() => {
          this.router.navigate(['Inicio/Cliente']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })


    }else{

      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });
      
      setTimeout(() => {
        this.router.navigate(['/Editar/Cliente']).then(() => {
          window.location.reload();
        });
      }, 1000); 
      
    }
  } 
}
