import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemotlComponent } from './memotl.component';

describe('MemotlComponent', () => {
  let component: MemotlComponent;
  let fixture: ComponentFixture<MemotlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemotlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MemotlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
