import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceVentaService } from 'src/app/services/ServiceVenta/service-venta.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent {
  selectedValue='';
  datosc: any[] = [];
  datos: any[] = [];
  dato: any[] = [];
  dato1: any[] = [];
  datonvo: any[] = [];
  ticket: any[] = []
  objectId = ''
  currentDate: Date | undefined;
  sub = 0
  iva = 0
  total = 0
  contenido = 0
  cont = 0
  productoSchema = {
    cliente:'',
    productoVenta: [],
    subtotal: 0,
    iva: 0,
    total: 0
  }

  newSchemaProducto = {
    nombreProducto: '',
    marcaProducto: '',
    presentacionProducto: '',
    contenidoProducto: '',
    precioProducto: '',
    descripcionProducto: '',
    statusProducto: '',
  }
  constructor(private _productos: ServiceVentaService, private router: Router) { }

  ngOnInit(): void {
    this.currentDate = new Date();
    this.cargarVenta();
  }

  eliminar(id:any){
    this._productos.informacionProductoId(id).subscribe((data: any) => {
      this.dato1 = [data.respuesta]
    this.contenido = Number(this.dato1[0].contenidoProducto)
    this.cont = this.contenido + 1
    console.log(this.cont);
    this.newSchemaProducto = {
      nombreProducto: this.dato1[0].nombreProducto,
      marcaProducto: this.dato1[0].marcaProducto,
      presentacionProducto: this.dato1[0].presentacionProducto,
      contenidoProducto: String(this.cont),
      precioProducto: this.dato1[0].precioProducto,
      descripcionProducto: this.dato1[0].descripcionProducto,
      statusProducto: this.dato1[0].statusProducto,
    };
    this.updateProducto( this.objectId,this.newSchemaProducto)
    this.cargarVenta();
  });
  }

  obtener(id: any) {
    this._productos.informacionProductoId(id).subscribe((data: any) => {
      this.dato = [data.respuesta]
      this.contenido = Number(data.respuesta.contenidoProducto)
      if (this.contenido != 0) {
        this.cont = this.contenido - 1
        this.datonvo.push(data.respuesta);
        this.objectId = data.respuesta._id
        this.newSchemaProducto = {
          nombreProducto: this.dato[0].nombreProducto,
          marcaProducto: this.dato[0].marcaProducto,
          presentacionProducto: this.dato[0].presentacionProducto,
          contenidoProducto: String(this.cont),
          precioProducto: this.dato[0].precioProducto,
          descripcionProducto: this.dato[0].descripcionProducto,
          statusProducto: this.dato[0].statusProducto,
        };
        console.log(this.newSchemaProducto);

        console.log("ticket", this.datonvo);
        this.insertar(this.datonvo)
        this.updateProducto( this.objectId,this.newSchemaProducto)
        this.cargarVenta();

      } else {
        alertaError('Error no hay productos disponibles.');
      }
      this.contenido = 0
      this.cont = 0
      console.log(this.productoSchema.cliente);
      this.generarticket(this.datonvo)
    });
  }
  updateProducto(id:any,schema:any) {
    this._productos.actualizarProducto(id,schema).subscribe((data: any) => {
      console.log(data);
    })
  }
  insertar(arreglo: any) {
    this.ticket = arreglo
    let num = 0
    this.ticket.forEach(element => {
      num = num + Number(element.precioProducto)
    });
    this.sub = num
  }

  borrar(id: any) {
    this.objectId = this.datonvo[id]._id
    this.eliminar(this.objectId)
      this.datonvo.splice(id, 1);
      this.insertar(this.datonvo)
      this.cargarVenta();
      this.generarticket(this.datonvo)
  }

  imprimir() {
    this.addProducto()
    setTimeout(() => {
      this.router.navigate(['/Inicio/Menu2']).then(() => {
        window.location.reload();
      });
    }, 1000);
  }

  generarticket(arreglo: any){
    this.productoSchema.productoVenta = arreglo
    this.productoSchema.subtotal = this.sub
    this.productoSchema.iva = this.sub * .16
    this.productoSchema.total = this.sub + this.productoSchema.iva
  }


  addProducto() {
    console.log(this.productoSchema);
    this._productos.agregarVenta(this.productoSchema).subscribe((data: any) => {
      console.log("Datos guardados: ", data);
    })
      Swal.fire({
        icon: 'success',
        title: '¡Venta realizada!',
        timer : 20000
      });
      
  }
  cargarVenta() {
    this._productos.obtenerCliente().subscribe((data: any) => {
      this.datosc = data.respuesta;
      console.log(this.datosc);
    })
    this._productos.obtenerVenta().subscribe((data: any) => {
      this.datos = data.respuesta;
      console.log(this.datos);
    })
  }

  borrarVenta(id: any) {

    Swal.fire({
      title: '¡ADVERTENCIA!',
      text: '¿Desea eliminar de forma permanente el producto?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#31f700',
      cancelButtonColor: '#f70000',
      confirmButtonText: 'ACEPTAR',
      cancelButtonText: 'CANCELAR'
    }).then((result) => {
      if (result.isConfirmed) {
        this._productos.eliminarVenta(id).subscribe((data: any) => {
          console.log(data);
          this.cargarVenta();
        })
      }
    })
  }

  guardarId(id: any) {
    const objectId = id;
    localStorage.setItem('objectId', objectId);
    console.log("Id del usuario: ", objectId);
  }

}
function alertaError(message: string) {
  setTimeout(() => {
    Swal.fire({
      icon: 'error',
      title: '¡Error!',
      text: message,
      confirmButtonColor: '#561113',
      confirmButtonText: 'Aceptar'
    });
  }, 50);
  }