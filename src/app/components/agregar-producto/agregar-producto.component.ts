import { Component, OnInit } from '@angular/core';
import { ProductoServicioService } from 'src/app/services/ServiceProductos/producto-servicio.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agregar-producto',
  templateUrl: './agregar-producto.component.html',
  styleUrls: ['./agregar-producto.component.css']
})
export class AgregarProductoComponent implements OnInit{

  datos : any [] =[]

  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ\s]+$/;
  numeros = /^[1-9]\d{1,10}$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;
  url = /^(ftp|http|https):\/\/[^ "]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;
  ok4 = true;
  ok5 = true;
  ok6 = true;
  ok7 = true;
  datosc: any[] = [];
  newSchemaProducto = {
    nombreProducto : '',
    marcaProducto : '',
    presentacionProducto:'',
    contenidoProducto:'',
    precioProducto: '',
    descripcionProducto: '',
    proovedor:''
  }

  constructor(private _Producto : ProductoServicioService, private router : Router){}

  ngOnInit(): void {
    this.cargarprov()
  }
  cargarprov(){
    this._Producto.obtenerProvedor().subscribe((data: any) => {
      this.datosc = data.respuesta;
      console.log(this.datosc);
    })
  }
  cargarProductos(){
    this._Producto.obtenerProductos().subscribe((data : any ) => {
      this.datos = data.resultados;
      console.log(this.datos);
    })
  }

  addProducto(){
    this.ok4 = this.numeros.test(this.newSchemaProducto.contenidoProducto);
    this.ok5 = this.numeros.test(this.newSchemaProducto.precioProducto);
    

    if ([this.ok4, this.ok5].every(val => val === true)){
      this._Producto.agregarProducto(this.newSchemaProducto).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡PRODUCTO ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/Productos']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/Producto']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }
}