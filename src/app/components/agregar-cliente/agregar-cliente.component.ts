import { Component } from '@angular/core';
import { ServiceClienteService } from 'src/app/services/ServiceCliente/service-cliente.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.css']
})
export class AgregarClienteComponent {

  datos : any [] =[]

  letras = /^[A-Za-z0-9ÑñáéíóúÁÉÍÓÚ#.\s]+$/;
  numeros = /^[1-9]\d*(\.\d+)?$/;
  estatus = /^[A-Za-zÑñáéíóúÁÉÍÓÚ\s]+$/;

  ok1 = true;
  ok2 = true;
  ok3 = true;

  newSchemaCliente = {
    nombreCliente : '',
    telefonoCliente: '',
    direccionCliente: '',
  }

  constructor(private _Cliente : ServiceClienteService , private router : Router){}

  ngOnInit(): void {
  }

  cargarClientes(){
    this._Cliente.obtenerCliente().subscribe((data : any ) => {
      this.datos = data.resultados;
      console.log(this.datos);
    })
  }

  addCliente(){
    this.ok1 = this.letras.test(this.newSchemaCliente.nombreCliente);
    this.ok2 = this.numeros.test(this.newSchemaCliente.telefonoCliente);
    this.ok3 = this.letras.test(this.newSchemaCliente.direccionCliente);
    if ([this.ok1, this.ok2, this.ok3].every(val => val === true)){
      this._Cliente.agregarCliente(this.newSchemaCliente).subscribe((data : any) => {
        console.log("Datos guardados: ", data);
        
        Swal.fire({
          icon: 'success',
          title: '¡Cliente ALMACENADO!',
          timer : 20000
        });
        
        setTimeout(() => {
          this.router.navigate(['/Inicio/Cliente']).then(() => {
            window.location.reload();
          });
        }, 1000); 

      })
    }else{
      Swal.fire({
        icon: 'warning',
        title: '¡DATOS INCORRECTOS!',
        confirmButtonText: 'REGRESAR',
        timer : 8000
      });

      setTimeout(() => {
        this.router.navigate(['/Agregar/Cliente']).then(() => {
          window.location.reload();
        });
      }, 1000);
    }
  }
}


