import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'
import { AppComponent } from './app.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { VerProductosComponent } from './components/ver-productos/ver-productos.component';
import { VerProvedorComponent } from './components/ver-provedor/ver-provedor.component';
import { AppRoutingModule } from './app-routing.module';
import { AgregarProductoComponent } from './components/agregar-producto/agregar-producto.component';
import { FormsModule } from '@angular/forms';
import { EditarProductoComponent } from './components/editar-producto/editar-producto.component';
import { AgregarProveedorComponent } from './components/agregar-proveedor/agregar-proveedor.component';
import { EditarProveedorComponent } from './components/editar-proveedor/editar-proveedor.component';
import { VentaComponent } from './components/venta/venta.component';
import { VerClienteComponent } from './components/ver-cliente/ver-cliente.component';
import { AgregarClienteComponent } from './components/agregar-cliente/agregar-cliente.component';
import { EditarClienteComponent } from './components/editar-cliente/editar-cliente.component';
import { LoginAdminComponent } from './components/login-admin/login-admin.component';
import { LoginUsuarioComponent } from './components/login-usuario/login-usuario.component';
import { AgregarAdminComponent } from './components/agregar-admin/agregar-admin.component';
import { AgregarUsuarioComponent } from './components/agregar-usuario/agregar-usuario.component';
import { MemotlComponent } from './components/memotl/memotl.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    VerProductosComponent,
    VerProvedorComponent,
    AgregarProductoComponent,
    EditarProductoComponent,
    AgregarProveedorComponent,
    EditarProveedorComponent,
    VentaComponent,
    VerClienteComponent,
    AgregarClienteComponent,
    EditarClienteComponent,
    LoginAdminComponent,
    LoginUsuarioComponent,
    AgregarAdminComponent,
    AgregarUsuarioComponent,
    MemotlComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
